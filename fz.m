function [ z ] = fx( x,y )

global H a b  ;
z=H-a*x.^2-b*y.^4+cos(x)/4; 
end